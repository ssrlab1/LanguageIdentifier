<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	
	include_once 'LanguageIdentifier.php';
	LanguageIdentifier::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text)) {
		$LanguageIdentifier = new LanguageIdentifier();
		$LanguageIdentifier->setText($text);
		$langArr = $LanguageIdentifier->lang_detect($text);
		$LanguageIdentifier->saveCacheFiles();
		
		$result['text'] = $text;
		$result['result'] = LanguageIdentifier::showMessage($langArr[0]);
		$msg = json_encode($result);
	}
	echo $msg;
?>