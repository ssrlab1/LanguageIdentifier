<?php
	mb_internal_encoding("UTF-8");
	mb_regex_encoding("UTF-8");
	
	function sendPost($data) {
		$curl = curl_init();
		$url = "https://corpus.by/LanguageIdentifier/api.php";
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_USERAGENT => "From Front End",
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_SSL_VERIFYPEER => 0
		));
		$result = curl_exec($curl);
		curl_close($curl); 
		return $result;
	}
	
	$br = "<br />\n";
	$allTestsCnt = 0;
	$activatedTestsCnt = 0;
	$errorCnt = 0;
	$errorList = "";
	$resultStatistics = "";

	$start = time();
	$tests = array();
	$testsCnt = $paramCnt = 0;
	$filepath = dirname(__FILE__) . "/tests/tests.txt";
	$handle = fopen($filepath, "r") OR die("'Памылка пры адкрыцці файла з тэстамі!'");
	if($handle) {
		while(!feof($handle))  {
			$line = trim(fgets($handle));
			if(substr($line, 0, 1) != "#" && substr($line, 0, 2) != "//") {
				if($line == "") {
					$paramCnt = 0;
					$testsCnt++;
				}
				else {
					if($line == "null") { $line = ""; }
					if($paramCnt == 0) { $tests[$testsCnt]["activation"] = $line; $paramCnt++; continue; }
					if($paramCnt == 1) { $tests[$testsCnt]["text"] = $line; $paramCnt++; continue; }
					if($paramCnt == 2) { $tests[$testsCnt]["localization"] = $line; $paramCnt++; continue; }
					if($paramCnt == 3) { $tests[$testsCnt]["requiredResult"] = $line; $paramCnt++; continue; }
				}
			}
		}
	}
	fclose($handle);
	
	foreach($tests as $testNumber => $test) {
		$allTestsCnt++;
		if($test["activation"]) {
			$activatedTestsCnt++;
			$input = $test["text"];
			$result = json_decode(sendPost($test), true);
			$resultCurrent = htmlspecialchars(str_replace("\n", '\\n', trim($result["result"])));
			$resultRequired = htmlspecialchars(trim($test["requiredResult"]));
			if($resultCurrent != $resultRequired) {
				$errorCnt++;
				$maxLenght = mb_strlen($resultCurrent) > mb_strlen($resultRequired) ? mb_strlen($resultCurrent) : mb_strlen($resultRequired);
				$mismatchIndex = -1;
				for($i = 0; $i < $maxLenght; $i++) {
					$char1 = mb_substr($resultCurrent, $i, 1, "UTF-8");
					$char2 = mb_substr($resultRequired, $i, 1, "UTF-8");
					if(strcmp($char1, $char2) != 0) {
						$mismatchIndex = $i;
						break;
					}
				}
				if($mismatchIndex >= 0) {
					$coincidedPart = mb_substr($resultCurrent, 0, $mismatchIndex, "UTF-8");
				}
				
				$resultRequired = $coincidedPart . '<font color="red">' . mb_substr($resultRequired, $mismatchIndex) . '</font>';
				$resultCurrent = $coincidedPart . '<font color="red">' . mb_substr($resultCurrent, $mismatchIndex) . '</font>';
				$coincidedPart = $coincidedPart;
				
				$errorList .= "<b>Wrong test! (line = $testNumber)</b>$br";
				$errorList .= "<b>Input:</b> $input$br";
				$errorList .= "<b>Coincided part ... :</b> $coincidedPart$br";
				$errorList .= "<b>Required ............ :</b> $resultRequired$br";
				$errorList .= "<b>Now out ............. :</b> $resultCurrent$br";
				$errorList .= $br . $br;
			}
		}
	}
	$duration = time() - $start;
	
	if($allTestsCnt > 0) {
		$activatedTestsPercentage = round($activatedTestsCnt / $allTestsCnt * 100, 2);
		$successfulTestsCnt = $activatedTestsCnt - $errorCnt;
		$successfulTestsPercentage = round($successfulTestsCnt / $activatedTestsCnt * 100, 2);
		
		$resultStatistics .= "Дакладнасць працы сэрвіса: <b>$successfulTestsPercentage % ($successfulTestsCnt з $activatedTestsCnt)</b>. ";
		$resultStatistics .= "Працягласць тэставання: <b>$duration с</b>. ";
		$resultStatistics .= "Актываваных тэстаў: <b>$activatedTestsPercentage % ($activatedTestsCnt з $allTestsCnt)</b>. ";
		$resultStatistics .= "Усяго тэстаў: <b>$allTestsCnt</b>.$br$br";
		
		echo $resultStatistics;
		echo $errorList;
		echo 'Файл з тэстамі можна паглядзець <a href="../_core/showTests.php?s=LanguageIdentifier">ТУТ</a>.' . $br . $br;
	}
	else {
		echo "Не знойдзена ніводнага тэста." . $br . $br;
	}
?>
