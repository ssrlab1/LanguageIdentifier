title
Language Identifier

help
https://ssrlab.by/en/6651

input
Please, enter a text or select an example for recognition :

min_size
*minimum length of the text is 80 characters

refresh
Refresh

clear
Clear

default input
Had I the heavens’ embroidered cloths,\nEnwrought with golden and silver light,\nThe blue and the dim and the dark cloths\nOf night and light and the half-light,\nI would spread the cloths under your feet:\nBut I, being poor, have only my dreams;\nI have spread my dreams under your feet;\n Tread softly because you tread on my dreams.

default input ru
Вы помните,\nВы всё, конечно, помните,\nКак я стоял,\nПриблизившись к стене,\nВзволнованно ходили вы по комнате\nИ что-то резкое\nВ лицо бросали мне.\n\nВы говорили:\nНам пора расстаться,\nЧто вас измучила\nМоя шальная жизнь,\nЧто вам пора за дело приниматься,\nА мой удел -\nКатиться дальше, вниз.\n\nЛюбимая!\nМеня вы не любили.\nНе знали вы, что в сонмище людском\nЯ был как лошадь, загнанная в мыле,\nПришпоренная смелым ездоком.\n\nНе знали вы,\nЧто я в сплошном дыму,\nВ развороченном бурей быте\nС того и мучаюсь, что не пойму -\nКуда несет нас рок событий.

default input be
Груша цвіла апошні год.\nУсе галіны яе, усе вялікія расохі, да апошняга пруціка, былі ўсыпаны буйным бела-ружовым цветам. Яна кіпела, млела і раскашавалася ў пчаліным звоне, цягнула да сонца сталыя лапы і распускала ў яго ззянні маленькія, кволыя пальцы новых парасткаў. І была яна такая магутная і свежая, так утрапёна спрачаліся ў яе ружовым раі пчолы, што, здавалася, не будзе ёй зводу і не будзе ёй канца.

default input en
Had I the heavens embroidered cloths,\nEnwrought with golden and silver light,\nThe blue and the dim and the dark cloths\nOf night and light and the half-light,\nI would spread the cloths under your feet:\nBut I, being poor, have only my dreams;\nI have spread my dreams under your feet;\n Tread softly because you tread on my dreams.

default input ua
Стаття 26.\n1) Кожна людина має право на освіту. Освіта повинна бути безплатною, хоча б початкова і загальна. Початкова освіта повинна бути обов'язковою. Технічна і професійна освіта повинна бути загальнодоступною, а вища освіта повинна бути однаково доступною для всіх на основі здібностей кожного.\n2) Освіта повинна бути спрямована на повний розвиток людської особи і збільшення поваги до прав людини і основних свобод. Освіта повинна сприяти взаєморозумінню, терпимості і дружбі між усіма народами, расовими, або релігійними групами і повинна сприяти діяльності Організації Об'єднаних Націй по підтриманню миру.\n3) Батьки мають право пріоритету у виборі виду освіти для своїх малолітніх дітей.

default input ger
Ludwig van Beethoven wurde 1770 in der kleinen Stadt Bonn am Rhein als Sohn eines Choristen geboren. Sein Groβvater war auch Musiker. Ludwig van Beethoven wurde nach seinem Groβvater Ludwig genannt. Sein Porträt begleitete den Komponisten bis in seine letzte Wohnung. \nSeine musikalische Begabung zeigte sich sehr früh. Der Kleine wurde gezwungen, täglich stundenlang am Klavier zu silzen, um als Wunderkind Aufsehen zu erregen, damit der Vater aus dem Talent des Kleinen Nutzen ziehen konnte. \nAls Beethoven sechs Jahre alt war, spielte er zum erstenmal in einem Konzert, und schon im Alter von 13 Jahren wurde er im Theaterorchester fest angestellt. Er spielte Orgel, Klavirzimbel, Geige und machte Versuche, zu komponieren. 

options
Options

check1
Return the entire result table

check2
Use heuristics

check3
Use ONLY rules

check4
Rules are more important than statistics

check5
Require all rules to coincide to determine (otherwise - any rule)

check6
Take into account the total length of the text of each alphabet or the percentage of use of the letters included in it

rez1
Primary language of the text

be
Belarusian

en
English

ru
Russian

ua
Ukraine 

ger
German

fr
French

button
Define language!

result
Result

service code
Service source code can be downloaded

reference
here

suggestions
Suggest an improvement of the service

contact e-mail
We would be glad to receive your feedback and suggestions via e-mail

other prototypes
Our other prototypes:

laboratory
Speech synthesis and recognition laboratory, UIIP NAS Belarus
