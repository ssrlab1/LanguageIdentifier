<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'LanguageIdentifier.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = LanguageIdentifier::loadLanguages();
	LanguageIdentifier::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo LanguageIdentifier::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextBe = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', LanguageIdentifier::showMessage('default input be'))); ?>";
			var inputTextRu = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', LanguageIdentifier::showMessage('default input ru'))); ?>";
			var inputTextUa = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', LanguageIdentifier::showMessage('default input ua'))); ?>";
			var inputTextGer = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', LanguageIdentifier::showMessage('default input ger'))); ?>";
			var inputTextEn = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', LanguageIdentifier::showMessage('default input en'))); ?>";
			var inputTextDefault = inputTextBe;
			$(document).ready(function() {
				$('#langId').change(function() {
					if($('#langId').val()=='be') inputTextDefault = inputTextBe;
					if($('#langId').val()=='ru') inputTextDefault = inputTextRu;
					if($('#langId').val()=='en') inputTextDefault = inputTextEn;
					if($('#langId').val()=='ger') inputTextDefault = inputTextGer;
					if($('#langId').val()=='ua') inputTextDefault = inputTextUa;
					document.getElementById('inputTextId').value=inputTextDefault;
				})
			});
			$(document).ready(function () {
				$('button#MainButtonId').click(function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/LanguageIdentifier/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val()
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var output = '<b><?php echo LanguageIdentifier::showMessage('rez1'); ?>:</b> ' + result.result;
							$('#resultId').html(output);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/LanguageIdentifier/?lang=<?php echo $lang; ?>"><?php echo LanguageIdentifier::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo LanguageIdentifier::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo LanguageIdentifier::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = LanguageIdentifier::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . LanguageIdentifier::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<form class="form-inline pull-right">
									<select name='lang' id="langId" class="form-control form-control-sm" style="width: 150px;">
										<option value="be"><?php echo LanguageIdentifier::showMessage('be'); ?></option> 
										<option value="ru"><?php echo LanguageIdentifier::showMessage('ru'); ?></option> 
										<option value="ua"><?php echo LanguageIdentifier::showMessage('ua'); ?></option> 
										<option value="en"><?php echo LanguageIdentifier::showMessage('en'); ?></option> 
										<option value="ger"><?php echo LanguageIdentifier::showMessage('ger'); ?></option> 
									</select>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo LanguageIdentifier::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo LanguageIdentifier::showMessage('clear'); ?></button>
								</form>
								<h3 class="panel-title"><?php echo LanguageIdentifier::showMessage('input'); ?></h3>(<?php echo LanguageIdentifier::showMessage('min_size'); ?>)
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", LanguageIdentifier::showMessage('default input be')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo LanguageIdentifier::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo LanguageIdentifier::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo LanguageIdentifier::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/LanguageIdentifier" target="_blank"><?php echo LanguageIdentifier::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/LanguageIdentifier/-/issues/new" target="_blank"><?php echo LanguageIdentifier::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo LanguageIdentifier::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo LanguageIdentifier::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo LanguageIdentifier::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php LanguageIdentifier::sendErrorList($lang); ?>